#include <iostream>
#include <cuda_runtime.h>
#include "matrix-utils.h"
#include <cuda.h>
#include <cuda_runtime.h>

void quebraLinha() {
	std::cout << std::endl;
}

Matriz* gerarStructMatriz(int linhas, int colunas) {

	Matriz* structSendoGerada = (Matriz*) malloc(sizeof(Matriz));

	structSendoGerada->numeroColunas = colunas;

	structSendoGerada->numeroLinhas = linhas;

	structSendoGerada->tamanhoFloatArray = colunas * linhas * sizeof(float);

	structSendoGerada->matriz = (float*) malloc(colunas * linhas * sizeof(float));

	return structSendoGerada;
}

void printMatriz(Matriz* matriz) {
	float* posicaoAcessada;

	for (int contadorLinhas = 0; contadorLinhas < matriz->numeroLinhas;
			contadorLinhas++) {
		for (int contadorColunas = 0; contadorColunas < matriz->numeroColunas;
				contadorColunas++) {

			acessaVetorComoMatrix(&posicaoAcessada, contadorLinhas,
					contadorColunas, matriz);

			*posicaoAcessada < 10 ? std::cout << "0000" :
			*posicaoAcessada < 100 ? std::cout << "000" :
			*posicaoAcessada < 1000 ? std::cout << "00" :
			*posicaoAcessada < 10000 ? std::cout << "0" : std::cout << "";

			std::cout << *posicaoAcessada << " ";
		}
		quebraLinha();
	}
}

void desalocaStructMatriz(Matriz* matriz) {
	free(matriz->matriz);
	free(matriz);
}

void preencherMatrizComNumeracaoCreescente(Matriz* matriz) {
	float* posicaoDesejada;

	for (int contadorLinha = 0; contadorLinha < matriz->numeroLinhas;
			contadorLinha++) {

		for (int contadorColuna = 0; contadorColuna < matriz->numeroColunas;
				contadorColuna++) {

			acessaVetorComoMatrix(&posicaoDesejada, contadorLinha,
					contadorColuna, matriz);

			*posicaoDesejada = (contadorLinha * matriz->numeroColunas)
					+ contadorColuna;
		}
	}
}

bool verificarMultMatrizVetor(Matriz* m, Matriz* v) {
	if (m->numeroColunas == v->numeroLinhas) {
		return true;
	}
	return false;
}

void alocarMatrizCuda(Matriz* matriz, float* ponteiro) {
//	cudaMalloc(&matriz, m->size);
}

void simularMultiplicacaoNoHost(Matriz* matriz, Matriz* vetor) {
	if (!verificarMultMatrizVetor(matriz, vetor)) {
		std::cout
				<< "Impossível fazer simulação com matrizes do tamanho do passado";
		quebraLinha();
		return;
	} else {
		std::cout << "Simulando multiplicação no host!";
		quebraLinha();
	}

	Matriz* resultante = gerarStructMatriz(matriz->numeroLinhas,
			vetor->numeroColunas);

	/* Realizando multiplicação no host **/
	float *posicaoAcessadaMatrizResultante, *posicaoAcessadaVetor,
			*posicaoAcessadaMatriz;
	for (unsigned int varredor = 0; varredor < matriz->numeroColunas;
			varredor++) {
		for (unsigned int contadorLinha = 0;
				contadorLinha < resultante->numeroLinhas; contadorLinha++) {
			for (unsigned int contadorColuna = 0;
					contadorColuna < resultante->numeroColunas;
					contadorColuna++) {
				acessaVetorComoMatrix(&posicaoAcessadaMatrizResultante,
						contadorLinha, contadorColuna, resultante);

				acessaVetorComoMatrix(&posicaoAcessadaMatriz, contadorLinha,
						varredor, matriz);

				acessaVetorComoMatrix(&posicaoAcessadaVetor, varredor,
						contadorColuna, vetor);

				*posicaoAcessadaMatrizResultante =
						*posicaoAcessadaMatrizResultante
								+ *posicaoAcessadaMatriz
										* *posicaoAcessadaVetor;
			}
		}
	}

	// Exibe a matriz resultante do host após o calculo
	std::cout << "Exibe martiz resultante host após o processamento [HOST]";
	quebraLinha();
	printMatriz(resultante);
	desalocaStructMatriz(resultante);

	/* Realizando multiplicação no host **/
}

__host__ __device__ void acessaVetorComoMatrix(
		float** ponteiroDaPosicaoDesejada, unsigned int linha,
		unsigned int coluna, Matriz* matrixAcessada) {

	if (linha > matrixAcessada->numeroLinhas
			|| coluna > matrixAcessada->numeroColunas) {

		*ponteiroDaPosicaoDesejada =
				&(matrixAcessada->matriz[matrixAcessada->numeroColunas
						* matrixAcessada->numeroLinhas - 1]);

	} else {

		*ponteiroDaPosicaoDesejada = &(matrixAcessada->matriz[linha
				* matrixAcessada->numeroColunas + coluna]);

	}
}
