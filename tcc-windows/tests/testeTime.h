#include <stdio.h>
#include <cuda.h>


__global__ void matrixMulti(unsigned int numeroLinhasMatriz,
		unsigned int numeroColunasMatriz, float* matriz,
		unsigned int numeroLinhasVetor, unsigned int numeroColunasVetor,
		float* vetor, float* resultante);


void testeTime();