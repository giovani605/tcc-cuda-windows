#include <iostream>
#include <cuda.h>

#include <cuda_runtime.h>
#include <time.h>
#include <stdio.h>

#include <iostream>
#include "testeTime.h"
#include "../matrix-utils/matrix-utils.h"
#include <chrono>

#include <vector> 




__global__ void matrixMulti(unsigned int numeroLinhasMatriz,
		unsigned int numeroColunasMatriz, float* matriz,
		unsigned int numeroLinhasVetor, unsigned int numeroColunasVetor,
		float* vetor, float* resultante) {

	unsigned int posicaoResultante = threadIdx.x * numeroColunasVetor
			+ threadIdx.y;
	unsigned int posicaoMatriz = threadIdx.x * numeroColunasMatriz + blockIdx.x;
	unsigned int posicaoVetor = blockIdx.x * numeroColunasVetor + threadIdx.y;

	float valorResultante = resultante[posicaoResultante], valorMatriz =
			matriz[posicaoMatriz], valorVetor = vetor[posicaoVetor];

	resultante[posicaoResultante] = resultante[posicaoResultante]
			+ matriz[posicaoMatriz] * vetor[posicaoVetor];

}

typedef struct SpecTest
{
	int qtdTestes;
	int sizeMatrix;
};

void testeTime() {

	// init matriz
	// aloca o vetor

	auto tempo_create_matrix = std::chrono::high_resolution_clock::now();

	SpecTest* test = (SpecTest*)malloc(sizeof(SpecTest));
	test->qtdTestes = 100;
	test->sizeMatrix = 36000;
	
	Matriz* matrizDeMultiplicacao = gerarStructMatriz(test->sizeMatrix, test->sizeMatrix);

	// Gera e exibe o vetor a ser multiplicado
	Matriz* vetorDeMultiplicacao = gerarStructMatriz(test->sizeMatrix, 1);
	
	Matriz* matrizResultanteHost = gerarStructMatriz(
		matrizDeMultiplicacao->numeroLinhas,
		vetorDeMultiplicacao->numeroColunas);


	// Gera e exibe a matriz resultante da multiplicação
	Matriz* matrizResultanteDevice = gerarStructMatriz(
		matrizDeMultiplicacao->numeroLinhas,
		vetorDeMultiplicacao->numeroColunas);

	auto tempo_create_matrix_end = std::chrono::high_resolution_clock::now();
	int time = std::chrono::duration_cast<std::chrono::milliseconds>(tempo_create_matrix_end - tempo_create_matrix).count();
	std::cout << "tempo create matrix " << time << " milliseconds" << std::endl;

	// copiar matriz para a GPU

	// Declara os ponteiro a serem passados para o device
	float* testeMalloc;
	float* matriz;
	float* vetor;
	float* resultante;
	int cudaStatus;
	std::cout << " tamanho da matriz " << matrizDeMultiplicacao->tamanhoFloatArray / 1000000 << " MBs"<< std::endl;

	// Aloca as variávis relacionadas a matriz a ser multiplicada no device
	cudaStatus =  cudaMalloc(&matriz, matrizDeMultiplicacao->tamanhoFloatArray);
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cudaMemcpy failed!");
	}
	// Aloca as variáveis relacionadas ao vetor a ser multiplicado no device
	cudaMalloc(&vetor, vetorDeMultiplicacao->tamanhoFloatArray);

	// Aloca a variável relaciona a matriz de reposta da multiplicação
	cudaMalloc(&resultante, matrizResultanteDevice->tamanhoFloatArray);


	auto tempo_transfer_init = std::chrono::high_resolution_clock::now();
	// Copia a matriz de multiplicação para a memória alocada no device

	
	cudaStatus = cudaMemcpy(matriz, matrizDeMultiplicacao->matriz,
		matrizDeMultiplicacao->tamanhoFloatArray, cudaMemcpyHostToDevice);
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cudaMemcpy failed!");
	}

	// Copia o vetor de multiplicação para a memória alocada no device
	cudaStatus = cudaMemcpy(vetor, vetorDeMultiplicacao->matriz,
		vetorDeMultiplicacao->tamanhoFloatArray, cudaMemcpyHostToDevice);

	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cudaMemcpy failed!");
	}
	

	dim3 numeroBlocos2D(matrizResultanteHost->numeroLinhas,
		matrizResultanteHost->numeroColunas);
	// rodar os teste e fazer a contagem
	auto tempo_transfer_end = std::chrono::high_resolution_clock::now();
	int time_create = std::chrono::duration_cast<std::chrono::milliseconds>(tempo_transfer_end - tempo_transfer_init).count();
	std::cout << "tempo transfer to GPU " << time_create << " milliseconds" << std::endl;
	std::vector<int> result;

	for (int a = 0; a < test->qtdTestes; a++) {

		// current date/time based on current system
		
		// convert now to string for

		auto start = std::chrono::high_resolution_clock::now();

		// operation to be timed ...

		

		 matrixMulti<<<matrizDeMultiplicacao->numeroColunas, numeroBlocos2D>>> (
			matrizDeMultiplicacao->numeroLinhas,
			matrizDeMultiplicacao->numeroColunas, matriz,
			vetorDeMultiplicacao->numeroLinhas,
			vetorDeMultiplicacao->numeroColunas, vetor, resultante);

		
		 cudaStatus =  cudaDeviceSynchronize();

		if (cudaStatus != cudaSuccess) {
			fprintf(stderr, "cudaMemcpy failed!");
		}

		std::cout << "Teste " << a << std::endl;
		auto finish = std::chrono::high_resolution_clock::now();
		int time = std::chrono::duration_cast<std::chrono::microseconds>(finish - start).count();
		result.push_back(time);

		std::cout << time << "microsegundos \n";
		quebraLinha();
	}
	
	for (int a : result) {
		std::cout << a << std::endl;
	}
	int Num2;
	// std::cin >> Num2;


	// salvar o tempo em um arquivo
	
}

