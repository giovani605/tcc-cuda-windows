#include <iostream>
#include <cuda.h>
#include <cuda_runtime_api.h>
#include <cuda_runtime.h>
#include "moscardisTestZone.h"
#include "../matrix-utils/matrix-utils.h"

void zonaDeTestes() {
	float *acessoComum;

	// Gera e exibe a matriz a ser multiplicada
	Matriz* matrizDeMultiplicacao = gerarStructMatriz(2, 3);

	// Preenche a matrix com valores conhecidos
	acessaVetorComoMatrix(&acessoComum, 0, 0, matrizDeMultiplicacao);
	*acessoComum = 0;
	acessaVetorComoMatrix(&acessoComum, 0, 1, matrizDeMultiplicacao);
	*acessoComum = 3;
	acessaVetorComoMatrix(&acessoComum, 0, 2, matrizDeMultiplicacao);
	*acessoComum = 5;
	acessaVetorComoMatrix(&acessoComum, 1, 0, matrizDeMultiplicacao);
	*acessoComum = 5;
	acessaVetorComoMatrix(&acessoComum, 1, 1, matrizDeMultiplicacao);
	*acessoComum = 5;
	acessaVetorComoMatrix(&acessoComum, 1, 2, matrizDeMultiplicacao);
	*acessoComum = 2;

	std::cout << "Exibe a matriz a ser multiplicada";
	quebraLinha();
	printMatriz(matrizDeMultiplicacao);

	// Gera e exibe o vetor a ser multiplicado
	Matriz* vetorDeMultiplicacao = gerarStructMatriz(3, 1);

	// Prenche vetor com valores conhecidos
	acessaVetorComoMatrix(&acessoComum, 0, 0, vetorDeMultiplicacao);
	*acessoComum = 3;
	acessaVetorComoMatrix(&acessoComum, 1, 0, vetorDeMultiplicacao);
	*acessoComum = 4;
	acessaVetorComoMatrix(&acessoComum, 2, 0, vetorDeMultiplicacao);
	*acessoComum = 3;

	std::cout << "Exibe o vetor a ser multiplicado";
	quebraLinha();
	printMatriz(vetorDeMultiplicacao);

	// Verifica se a multiplicação é possível
	if (!verificarMultMatrizVetor(matrizDeMultiplicacao,
			vetorDeMultiplicacao)) {
		std::cout
				<< "Impossível multiplicar matrizes com os tamanhos estipulados";
		quebraLinha();
		return;
	}

	// Gera e exibe a matriz resultante da multiplicação
	Matriz* matrizResultanteDevice = gerarStructMatriz(
			matrizDeMultiplicacao->numeroLinhas,
			vetorDeMultiplicacao->numeroColunas);
	std::cout << "Exibe matriz resultante antes do processamento";
	quebraLinha();
	printMatriz(matrizResultanteDevice);

	Matriz* matrizResultanteHost = gerarStructMatriz(
			matrizDeMultiplicacao->numeroLinhas,
			vetorDeMultiplicacao->numeroColunas);

	// Declara os ponteiro a serem passados para o device
	float *matriz, *vetor, *resultante;

	// Aloca as variáveis relacionadas a matriz a ser multiplicada no device
	cudaMalloc(&matriz, matrizDeMultiplicacao->tamanhoFloatArray);

	// Aloca as variáveis relacionadas ao vetor a ser multiplicado no device
	cudaMalloc(&vetor, vetorDeMultiplicacao->tamanhoFloatArray);

	// Aloca a variável relaciona a matriz de reposta da multiplicação
	cudaMalloc(&resultante, matrizResultanteDevice->tamanhoFloatArray);

	// Copia a matriz de multiplicação para a memória alocada no device
	cudaMemcpy(matriz, matrizDeMultiplicacao->matriz,
			matrizDeMultiplicacao->tamanhoFloatArray, cudaMemcpyHostToDevice);

	// Copia o vetor de multiplicação para a memória alocada no device
	cudaMemcpy(vetor, vetorDeMultiplicacao->matriz,
			vetorDeMultiplicacao->tamanhoFloatArray, cudaMemcpyHostToDevice);

	/* Aqui a mágica acontece **/

	/* Realizando multiplicação no host **/

	simularMultiplicacaoNoHost(matrizDeMultiplicacao, vetorDeMultiplicacao);

	/* Realizando multiplicação no host **/

	/* Realizando multiplicação no device **/
	dim3 numeroBlocos2D(matrizResultanteHost->numeroLinhas,
			matrizResultanteHost->numeroColunas);

	kernalDeMultiplicacao<<<matrizDeMultiplicacao->numeroColunas, numeroBlocos2D>>>(
			matrizDeMultiplicacao->numeroLinhas,
			matrizDeMultiplicacao->numeroColunas, matriz,
			vetorDeMultiplicacao->numeroLinhas,
			vetorDeMultiplicacao->numeroColunas, vetor, resultante);

	cudaDeviceSynchronize();

	// Copia a matriz resultante do device para o host
	cudaMemcpy(matrizResultanteDevice->matriz, resultante,
			matrizResultanteDevice->tamanhoFloatArray, cudaMemcpyDeviceToHost);

	std::cout << "Exibe martiz resultante host após o processamento [DEVICE]";
	quebraLinha();
	printMatriz(matrizResultanteDevice);

	/* Realizando multiplicação no device **/

	/* Aqui a mágica acontece **/

// Remove do device as alocações relacionadas a mratiz que foi multiplicada
	cudaFree(matriz);

// Remove do device as alocações relacionadas ao vetor que foi multiplicado
	cudaFree(vetor);

// Remove do device a alocação da matriz resultante
	cudaFree(resultante);

// Libera a memória do host
	desalocaStructMatriz(matrizDeMultiplicacao);
	desalocaStructMatriz(vetorDeMultiplicacao);
	desalocaStructMatriz(matrizResultanteDevice);
}

__global__ void kernalDeMultiplicacao(unsigned int numeroLinhasMatriz,
		unsigned int numeroColunasMatriz, float* matriz,
		unsigned int numeroLinhasVetor, unsigned int numeroColunasVetor,
		float* vetor, float* resultante) {

	unsigned int posicaoResultante = threadIdx.x * numeroColunasVetor
			+ threadIdx.y;
	unsigned int posicaoMatriz = threadIdx.x * numeroColunasMatriz + blockIdx.x;
	unsigned int posicaoVetor = blockIdx.x * numeroColunasVetor + threadIdx.y;

	float valorResultante = resultante[posicaoResultante], valorMatriz =
			matriz[posicaoMatriz], valorVetor = vetor[posicaoVetor];

	printf(
			"Posições sendo acessadas!\nResultante: [%d,%d] = %d\nMatriz: [%d,%d] = %d\nVetor: [%d,%d] = %d\nConta sendo realizada no device %f + %f * %f\n\n",
			threadIdx.x, threadIdx.y, posicaoResultante, threadIdx.x,
			blockIdx.x, posicaoMatriz, blockIdx.x, threadIdx.y, posicaoVetor,
			valorResultante, valorMatriz, valorVetor);

	resultante[posicaoResultante] = resultante[posicaoResultante]
			+ matriz[posicaoMatriz] * vetor[posicaoVetor];

}
